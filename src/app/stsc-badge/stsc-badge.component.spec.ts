import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StscBadgeComponent } from './stsc-badge.component';

describe('StscBadgeComponent', () => {
  let component: StscBadgeComponent;
  let fixture: ComponentFixture<StscBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StscBadgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StscBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
