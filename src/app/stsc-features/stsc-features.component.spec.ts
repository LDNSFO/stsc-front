import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StscFeaturesComponent } from './stsc-features.component';

describe('StscFeaturesComponent', () => {
  let component: StscFeaturesComponent;
  let fixture: ComponentFixture<StscFeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StscFeaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StscFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
