import { TestBed } from '@angular/core/testing';

import { StscEngineService } from './stsc-engine.service';

describe('StscEngineService', () => {
  let service: StscEngineService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StscEngineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
