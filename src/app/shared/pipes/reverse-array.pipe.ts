import { Pipe } from '@angular/core';

@Pipe({
  name: 'reverse'
})
export class ReversePipe {
  transform(value) {
      console.log(value)
      console.log(value.slice().reverse())

    return value.slice().reverse();
  }
}