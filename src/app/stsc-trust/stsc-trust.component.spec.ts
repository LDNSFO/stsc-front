import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StscTrustComponent } from './stsc-trust.component';

describe('StscTrustComponent', () => {
  let component: StscTrustComponent;
  let fixture: ComponentFixture<StscTrustComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StscTrustComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StscTrustComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
