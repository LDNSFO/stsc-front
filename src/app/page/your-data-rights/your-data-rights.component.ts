import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-your-data-rights',
  templateUrl: './your-data-rights.component.html',
  styleUrls: ['./your-data-rights.component.scss']
})

export class YourDataRightsComponent implements OnInit {

  constructor() { }

  ngOnInit() { }

  ngOnDestroy() { }
}