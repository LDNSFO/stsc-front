import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router'; // we also need angular router for Nebular to function properly

import { NbStepperModule, NbCardModule, NbLayoutModule, NbRadioModule, NbButtonModule, NbListModule, NbCheckboxModule } from '@nebular/theme';
import { NbIconModule, NbInputModule, NbSelectModule, NbFormFieldModule } from '@nebular/theme';

import { GenerateResetPasswordTokenComponent } from './generate-reset-password-token.component';

import { GlowService } from '../../glow.service';
import { DateFormatService } from '../../date-format.service';
import { SharedModule } from 'src/app/shared/shared.module';


const routes: Routes = [
  {
    path: '',
    component: GenerateResetPasswordTokenComponent
  }
];

@NgModule({
  declarations: [ GenerateResetPasswordTokenComponent ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    NbStepperModule,
    NbLayoutModule,
    NbCardModule,
    NbRadioModule,
    NbButtonModule,
    NbSelectModule,
    NbListModule,
    NbInputModule,
    NbCheckboxModule,
    NbFormFieldModule,
    NbIconModule,
    SharedModule
  ],
  providers: [
    DateFormatService,
    GlowService
  ]
})
export class GenerateResetPasswordTokenModule { }
