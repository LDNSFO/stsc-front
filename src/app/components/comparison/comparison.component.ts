import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-comparison',
  templateUrl: './comparison.component.html',
  styleUrls: ['./comparison.component.scss']
})

export class ComparisonComponent implements OnInit {

  constructor(private dialogRef: NbDialogRef<any>) { }

  ngOnInit() { }

  ngOnDestroy() { }

  close() {
    this.dialogRef.close();
  }
}