<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

$product_list = array(
"/tmp/product_AFFECT-FIX-12M-20-09-22.json",
"/tmp/product_AFFECT-VAR-20-10-01.json",
"/tmp/product_AGILE-18-02-21.json",
"/tmp/product_COOP-FIX-12M-20-09-22.json",
"/tmp/product_COOP-PP-VAR-20-04-01.json",
"/tmp/product_COOP-PP-WHD-VAR-20-04-01.json",
"/tmp/product_COOP-VAR-20-10-01.json",
"/tmp/product_CP-12M-20-09-22.json",
"/tmp/product_FE1Q0114-DD.json",
"/tmp/product_FE1Q0114-QC.json",
"/tmp/product_FE1Q0214-DD.json",
"/tmp/product_FE1Q0214-QC.json",
"/tmp/product_FE1Q0713-DD.json",
"/tmp/product_FE1Q0713-QC.json",
"/tmp/product_FE1QFS9-DD.json",
"/tmp/product_FE1QFS9-QC.json",
// "/tmp/product_FE1QFV1-DD.json",
// "/tmp/product_FE1QFV1-QC.json",
// "/tmp/product_FE2Q0114-DD.json",
// "/tmp/product_FE2Q0114-QC.json",
// "/tmp/product_FE2Q0214-DD.json",
// "/tmp/product_FE2Q0214-QC.json",
// "/tmp/product_FE2Q0713-DD.json",
// "/tmp/product_FE2Q0713-QC.json",
// "/tmp/product_FE2QFS9-DD.json",
// "/tmp/product_FE2QFS9-QC.json",
// "/tmp/product_FE2QFV1-DD.json",
// "/tmp/product_FE2QFV1-QC.json",
// "/tmp/product_FEDQ0114-DD.json",
// "/tmp/product_FEDQ0114-QC.json",
// "/tmp/product_FEDQ0214-DD.json",
// "/tmp/product_FEDQ0214-QC.json",
// "/tmp/product_FEDQ0713-DD.json",
// "/tmp/product_FEDQ0713-QC.json",
// "/tmp/product_FEDQFS9-DD.json",
// "/tmp/product_FEDQFS9-QC.json",
// "/tmp/product_FEDQFV1-DD.json",
// "/tmp/product_FEDQFV1-QC.json",
// "/tmp/product_FEHQ0114-DD.json",
// "/tmp/product_FEHQ0114-QC.json",
// "/tmp/product_FEHQ0214-DD.json",
// "/tmp/product_FEHQ0214-QC.json",
// "/tmp/product_FEHQ0713-DD.json",
// "/tmp/product_FEHQ0713-QC.json",
// "/tmp/product_FGQ-0114-DD.json",
// "/tmp/product_FGQ-0114-QC.json",
// "/tmp/product_FGQ-0713-DD.json",
// "/tmp/product_FGQ-0713-QC.json",
// "/tmp/product_FGQ-FS9-DD.json",
// "/tmp/product_FGQ-FS9-QC.json",
// "/tmp/product_FGQ-FV1-DD.json",
// "/tmp/product_FGQ-FV1-QC.json",
// "/tmp/product_FH1QFS9-DD.json",
// "/tmp/product_FH1QFS9-QC.json",
// "/tmp/product_FH1QFV1-DD.json",
// "/tmp/product_FH1QFV1-QC.json",
// "/tmp/product_FH2QFS9-DD.json",
// "/tmp/product_FH2QFS9-QC.json",
// "/tmp/product_FH2QFV1-DD.json",
// "/tmp/product_FH2QFV1-QC.json",
"/tmp/product_FIX-12M-20-09-21.json",
"/tmp/product_GO-18-06-12.json",
"/tmp/product_GO-3H-0030.json",
"/tmp/product_GO-3H-0130.json",
"/tmp/product_GO-3H-0230.json",
"/tmp/product_GO-3H-0330.json",
"/tmp/product_GO-3H-2030.json",
"/tmp/product_GO-3H-2130.json",
"/tmp/product_GO-3H-2230.json",
"/tmp/product_GO-3H-2330.json",
"/tmp/product_GO-4H-0030.json",
"/tmp/product_GO-4H-0130.json",
"/tmp/product_GO-4H-0230.json",
"/tmp/product_GO-4H-2030.json",
"/tmp/product_GO-4H-2130.json",
"/tmp/product_GO-4H-2230.json",
"/tmp/product_GO-4H-2330.json",
"/tmp/product_GO-5H-0030.json",
"/tmp/product_GO-5H-0130.json",
"/tmp/product_GO-5H-2030.json",
"/tmp/product_GO-5H-2130.json",
"/tmp/product_GO-5H-2230.json",
"/tmp/product_GO-5H-2330.json",
"/tmp/product_LP-CN-FIX-12M-20-11-26.json",
"/tmp/product_LP-CN-FIX-24M-20-11-26.json",
"/tmp/product_LP-CNT-FIX-12M-20-11-26.json",
"/tmp/product_LP-CNT-FIX-24M-20-11-26.json",
"/tmp/product_LP-FIX-12M-20-11-26.json",
"/tmp/product_M-AND-S-12M-20-09-21.json",
"/tmp/product_M-AND-S-VAR-20-10-01.json",
"/tmp/product_PP-WHD-VAR-20-04-01.json",
"/tmp/product_PREPAY-VAR-18-09-21.json",
"/tmp/product_SUPER-GREEN-12M-20-09-22.json");

function write_small_test() {
    // binary data test
    $consumption[1500000] = 104;
    $consumption[1500010] = 133;
    $consumption[1500020] = 0;
    $consumption[1500030] = 1;
    $consumption[1500040] = 65235;

    ksort($consumption);
    $keys = array_keys($consumption);
    $maxts = max($keys);
    $mints = min($keys);

    foreach($consumption as $key => $value) {
        $valObj[] = $value;
    }

    var_dump($valObj);

    // magic header that is 0x11 for STSC data, 0x0F for consumption
    $binaryheader = pack("C*", 0x11, 0x0F);
    $binaryts = pack("N*", $mints);
    $binarydata = pack("n*", ...$valObj);
    $fp = fopen("/tmp/test.bin", "w");
    // echo $binarydata;
    fwrite($fp, $binaryheader, 2);
    fwrite($fp, $binaryts, 4);
    fwrite($fp, $binarydata);
    fclose($fp);
}

function read_consumption($filename) {
 
    $annualconsumption = 0;
    $maxannualvalues = (365*48);
    $fp = fopen($filename, "r");

    if(!$fp) return -1;

    $headersig = unpack("C", fread($fp, 1));
    $headertype = unpack("C", fread($fp, 1));
    $mints = unpack("N", fread($fp, 4));

    $mintsInt = intval($mints[1]);

    while( $tmpval = unpack("n", fread($fp, 2))) {
        $bindata[$mintsInt] = $tmpval[1];
        if($maxannualvalues-- > 0) {
            $annualconsumption += $tmpval[1];
        }
        $mintsInt = $mintsInt + (30*60);
    }

    fclose($fp);

    // var_dump($headersig) . "\r\n";
    // var_dump($headertype) . "\r\n";
    // var_dump($mints) . "\r\n";
    // var_dump($bindata);

    if($annualconsumption == 0) $annualconsumption = -1;
    return array("starttime" => $mints[1], "annualconsumption" => $annualconsumption, "data" => $bindata);
}

function get_octopus_product_rates($filename, $gsp) {
    $product_json = file_get_contents($filename);
    // determine the type
    $product = json_decode($product_json, 1);

    $stsc_product["source"] = $product;
    $stsc_product["id"] = $product["code"];

    // Come back to economyX tariffs
    // if(isset($product["dual_register_electricity_tariffs"][$gsp]["direct_debit_monthly"])) {
    //     $stsc_product["type"] = "econx";
    //     $product_detail = $product["dual_register_electricity_tariffs"][$gsp]["direct_debit_monthly"];
    //     $stsc_product["standingcharge"] = $product_detail["standing_charge_inc_vat"];
    //     $stsc_product["rate"]["day_unitrate"] = $product_detail["day_unit_rate_inc_vat"];
    //     $stsc_product["rate"]["night_unitrate"] = $product_detail["night_unit_rate_inc_vat"];
    // } else if(isset($product["dual_register_electricity_tariffs"][$gsp]["porob"])) {
    //     $stsc_product["type"] = "econx";
    //     $product_detail = $product["dual_register_electricity_tariffs"][$gsp]["porob"];
    //     $stsc_product["standingcharge"] = $product_detail["standing_charge_inc_vat"];
    //     $stsc_product["rate"]["day_unitrate"] = $product_detail["day_unit_rate_inc_vat"];
    //     $stsc_product["rate"]["night_unitrate"] = $product_detail["night_unit_rate_inc_vat"];
    // }

    if($product["code"]== "AGILE-18-02-21") {
        $stsc_product["type"] = "dynamic";
        $product_detail = $product["single_register_electricity_tariffs"][$gsp]["direct_debit_monthly"];
        $stsc_product["standingcharge"] = $product_detail["standing_charge_inc_vat"];
        $fp = fopen("/tmp/csv_agile$gsp.csv", "r");
        while($line = fgets($fp)) {
            $tariff_line = explode(",", $line);
            $ts = date_timestamp_get(date_create($tariff_line[0]));
            //echo $ts . "\r\n";
            $tariff[$ts] = 0+trim($tariff_line[4]);
        }
        
        $stsc_product["rate"] = $tariff;
    } else if(substr($product["code"], 0, 2) == "GO") {
        $stsc_product["type"] = "tou";
        $product_detail = $product["single_register_electricity_tariffs"][$gsp]["direct_debit_monthly"];
        $stsc_product["standingcharge"] = $product_detail["standing_charge_inc_vat"];
        $unitratesURL = "https://api.octopus.energy/v1/products/" . $product["code"] . "/electricity-tariffs/" . $product_detail["code"] . "/standard-unit-rates";
        $tariff_json = file_get_contents($unitratesURL);
        $tariff = json_decode($tariff_json, 1);
        $stsc_product["rate"] = array($tariff["results"][0], $tariff["results"][1]);
    } else if(isset($product["single_register_electricity_tariffs"][$gsp]["direct_debit_monthly"])) {
        // echo "DD\r\n";
        $stsc_product["type"] = "flat";
        $stsc_product["paymenttype"] = "dd";
        $product_detail = $product["single_register_electricity_tariffs"][$gsp]["direct_debit_monthly"];
        $stsc_product["standingcharge"] = $product_detail["standing_charge_inc_vat"];
        $stsc_product["rate"]["unitrate"] = $product_detail["standard_unit_rate_inc_vat"];
    } else if(isset($product["single_register_electricity_tariffs"][$gsp]["prepayment"])) {
        // echo "Prepayment\r\n";
        $stsc_product["type"] = "flat";
        $stsc_product["paymenttype"] = "prepay";
        $product_detail = $product["single_register_electricity_tariffs"][$gsp]["prepayment"];
        $stsc_product["standingcharge"] = $product_detail["standing_charge_inc_vat"];
        $stsc_product["rate"]["unitrate"] = $product_detail["standard_unit_rate_inc_vat"];
    } else if(isset($product["single_register_electricity_tariffs"][$gsp]["porob"])) {
        // echo "Ondemand\r\n";
        $stsc_product["type"] = "flat";
        $stsc_product["paymenttype"] = "ondemand";
        $product_detail = $product["single_register_electricity_tariffs"][$gsp]["porob"];
        $stsc_product["standingcharge"] = $product_detail["standing_charge_inc_vat"];
        $stsc_product["rate"]["unitrate"] = $product_detail["standard_unit_rate_inc_vat"];
    }

    return $stsc_product;
}


function flat_cost($product, $consumption) {
    $cost = 0;
    $monthly_cost = [];
    // calc the consumption on the read, let monthly be done on front-end?
    $cost = (($consumption["annualconsumption"] * $product["rate"]["unitrate"])/1000)/100 + (365 * $product["standingcharge"])/100;

    return array("annual_cost" => $cost, "monthly_cost" => $monthly_cost, "unitrate" => $product["rate"]["unitrate"], "standingcharge" => $product["standingcharge"]);
}

function tou_cost($product, $consumption) {
    $i=0;
    $cost = 0;
    $monthly_cost = [];

    // var_dump($product);
    // fill in the rates for 48 slots
    foreach($product["rate"] as $key => $tou_slot) {
        $start = date_timestamp_get(date_create($tou_slot["valid_from"]));
        $end = date_timestamp_get(date_create($tou_slot["valid_to"]));
        for($k=$start; $k < $end; $k += (30*60)) {
            $tou_slot_rate[($k % (48*30*60))/(30*60)] = $tou_slot["value_inc_vat"];
            // echo ($k % (48*30*60))/(30*60) . ") " . $k . " = " . $tou_slot["value_inc_vat"] . "\r\n";
        }
    }

    // var_dump($tou_slot_rate);

    foreach($consumption["data"] as $key => $value) {
        if($i > (365*48)) break;
        // echo $product["id"] . " TOU: " . date("Y-m", $key) . " = " . $key . " = " . $value . "\r\n";
        $hhc = ($value/1000 * $tou_slot_rate[($key % (48*30*60))/(30*60)])/100 + $product["standingcharge"]/4800;
        $cost += $hhc;
        $monthly_cost[date("Y-m", $key)] += $hhc;
        $i++;
    }

    return array("annual_cost" => $cost, "monthly_cost" => $monthly_cost);
}

function dynamic_cost($product, $consumption) {
    $i=0;
    $cost = 0;
    $monthly_cost = [];

    // limit to 365 days or (365 * 48 values)
    foreach($consumption["data"] as $key => $value) {
        if($i > (365*48)) break;
        // echo $key . " = " . ($product["rate"][$key]/1000*$value)/100 . " + " . $product["standingcharge"]/4800 . "\r\n";
        $hhc = ($product["rate"][$key]/1000*$value)/100 + $product["standingcharge"]/4800;
        $cost += $hhc;
        $monthly_cost[date("Y-m", $key)] += $hhc;
        $i++;
    }

    return array("annual_cost" => $cost, "monthly_cost" => $monthly_cost);

}

if(!($output = file_get_contents("/tmp/compare_test.json"))) {
    $mydata = read_consumption("/tmp/test.bin");
    // $productrates = get_octopus_product_rates("/tmp/product_AGILE-18-02-21.json", "_A");
    // $productrates = get_octopus_product_rates("/tmp/product_SUPER-GREEN-12M-20-09-22.json", "_A");
    // $productrates = get_octopus_product_rates("/tmp/product_GO-3H-0030.json", "_A");

    foreach($product_list as $filename) {
        $productrates = get_octopus_product_rates($filename, "_A");
        // var_dump($mydata);
        // var_dump($productrates);

        if($productrates["type"] == "flat") {
            $costArray = flat_cost($productrates, $mydata);
        }

        if($productrates["type"] == "tou") {
            $costArray = tou_cost($productrates, $mydata);
        }

        if($productrates["type"] == "dynamic") {
            $costArray = dynamic_cost($productrates, $mydata);
        }

        $monthgraph = array();
        foreach($costArray["monthly_cost"] as $monthKey => $monthValue) {
            $monthgraph[] = array( strtotime($monthKey . "-01T00:00:00Z")*1000, round($monthValue, 2) );
        }

        echo $filename . " = " . count($monthgraph) . "\r\n";

        $productrates["source"]["annualcost"] = round($costArray["annual_cost"], 2);
        $productrates["source"]["monthlycost"] = $monthgraph;
        $productrates["source"]["ratetype"] = $productrates["type"];
        $productrates["source"]["unitrate"] = $costArray["unitrate"];
        $productrates["source"]["standingcharge"] = $costArray["standingcharge"];


        $output[] = $productrates["source"];
        //$output[] = array("id" => $productrates["id"], "source" => $productrates["source"], "annualcost" => round($cost, 2));
    }

    file_put_contents("/tmp/compare_test.json", json_encode($output));
    //echo json_encode($output);

} else {
    //echo $output;
}



?>