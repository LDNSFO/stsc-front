<?php

class consumptionData {
    // take in token from the client side and mine the daily data for more complex TOU tariffs

    // $token = $_GET["token"];

    public $token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbkhhc2giOiI2MjZkNTlmZTAwNDNjNDU4NThmM2Y0MmYzNDczMDYxOTM2ZWE4NDQxMzIxNWNlMmU5NWQwNzVjMGVhMzA2MWNjZWQ1YzQ0N2U5MDUwZDAyM2ZlOWQ1NjljOGNmNDhhZDUiLCJpYXQiOjE2MDk3ODI2NTUsImV4cCI6MTYxMDM4NzQ1NX0.FWmUJaqv10HZ2BY4ZI-48UufiT44vkge1Pov5lrFy50';
    // store the data, etc within a file that is the token name

    // will need to discover the resources linked to an account
    // https://api.glowmarkt.com/api/v0-1/resource/b965aad1-6dc2-4d38-a44e-29b65f0ce69a/readings?nulls=1&from=2020-08-01T00:00:00&to=2020-08-01T23:59:59&period=PT30M&function=sum


    private $url = 'https://api.glowmarkt.com/api/v0-1/resource';
    
    public $to = '2020-08-01T23:59:59';
    public $from = '2020-08-01T00:00:00';


    function fetchResource($resource, $from, $to) {

        $resource = 'b965aad1-6dc2-4d38-a44e-29b65f0ce69a';
        $queryparams = 'readings?nulls=1&period=PT30M&function=sum&from=' . $from . '&to=' . $to;

        $applicationId = 'b0f1b774-a586-4f72-9edd-27ead8aa7a8d';
        $headers = [
            'applicationId: ' . $applicationId,
            'token: ' . $this->token,
            'Content-Type: application/json',
            'Accept: application/json, text/plain, */*',
            'User-Agent: Smart Tariff, Smart Comparison V0.1'
        ];
        
        // check filesystem, if not there then get from URL
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->url . "/$resource/$queryparams");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $output = curl_exec($ch);
            curl_close($ch); 
            
            return $output;

    }

    function setToken() {

    }


}

$mydata = new consumptionData();

$to = date('Y-m-d') . 'T' . date('H:00:00');
$now = date(DATE_ISO8601);
$fromDate = date_create($now);
date_add($fromDate, date_interval_create_from_date_string('-1 days'));

$newFromDate = date_format($fromDate,DATE_ISO8601);
$from = strstr($newFromDate, "T", true) . "T00:00:00";

$data = json_decode($mydata->fetchResource('b965aad1-6dc2-4d38-a44e-29b65f0ce69a', $from, $to), 1);

for($i=0; $i < 38; $i++) {
    $tmp = strstr($newFromDate, "T", true) . "T00:00:00";
    $toDate = date_create($tmp);
    $fromDate = date_create($tmp);
    date_add($toDate, date_interval_create_from_date_string('-1 second'));
    date_add($fromDate, date_interval_create_from_date_string('-10 days'));
    $newToDate = date_format($toDate,DATE_ISO8601);
    $to = strstr($newToDate, "T", true) . "T23:59:59";
    $newFromDate = date_format($fromDate,DATE_ISO8601);
    $from = strstr($newFromDate, "T", true) . "T00:00:00";
    echo "$from - $to\r\n"; 
    $data = json_decode($mydata->fetchResource('b965aad1-6dc2-4d38-a44e-29b65f0ce69a', $from, $to), 1);
    foreach ($data['data'] as $key => $value) {
        $val = $value[1] * 1000;
        settype ($val, "int");
        if($val != $value[1] * 1000) {
            echo $key . "," . $value[1] . " " . $val . "\r\n";
        }
        $consumption[$value[0]] = $val;
    }
}

ksort($consumption);
$keys = array_keys($consumption);
$maxts = max($keys);
$mints = min($keys);

// magic header that is 0x11 for STSC data, 0x0F for consumption
$binaryheader = pack("C*", 0x11, 0x0F);
$binaryts = pack("N*", $mints);
$binarydata = pack("n*", ...$consumption);
$fp = fopen("/tmp/test.bin", "w");
// echo $binarydata;
fwrite($fp, $binaryheader, 2);
fwrite($fp, $binaryts, 4);
fwrite($fp, $binarydata);
fclose($fp);

$count = count($consumption);
echo "Wrote $mints - $maxts : $count\r\n";
$json_data = json_encode($consumption);
file_put_contents("/tmp/data.json", $json_data);
 // echo count($data['data']);
   
