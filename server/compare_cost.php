<?php

function sort_score($a,$b) {
        return $a['score'] < $b['score'];
}

function sort_cost($a,$b) {
        return $a['annualcost'] > $b['annualcost'];
}

function encode($criteria) {
        
        $result = 0;
        $k=0;

        foreach($criteria as $i => $value) {
                if($value) {
                        $result += pow(2, $k);
                }
                $k++;
        }

        return $result;
}

function encode_product($product) {

        // var_dump($product);

        $testvector["smart"] = isset($product["smart"]) ? true : true;
        $testvector["green"] = isset($product["is_green"]) ? $product["is_green"] : false;

        $testvector["fixed"] = isset($product["is_variable"]) ? false : true;
        $testvector["variable"] = isset($product["is_variable"]) ? $product["is_variable"] : false;
        
        if(substr($product["code"], 0, 5) == 'AGILE') {
                $testvector["dynamic"] = true;
        } else {
                $testvector["dynamic"] = false;
        }

        if((substr($product["code"], 0, 2) == 'GO') || $testvector["dynamic"] ) {
                $testvector["tou"] = true;
        } else {
                $testvector["tou"] = false;
        }        
        
        // $testvector["tou"] = isset($product["tou"]) ? true : false;
        // $testvector["dynamic"] = isset($product["dynamic"]) ? true : false;

        // $testvector["contract_none"] = false;
        // $testvector["contract_12"] = false;
        // $testvector["contract_24"] = false;

        if($product["term"] > 11) {
                if($product["term"] > 23) {
                        $testvector["contract_24"] = true;
                } else {
                        $testvector["contract_12"] = true;
                }
        } else {
                $testvector["contract_none"] = true;
        }

        // need to look at the layer below for exit fees
        $testvector["exit"] = isset($product["exit"]) ? true : false;

        $testvector["solar"] = isset($product["solar"]) ? true : false;

        if($testvector["tou"] ) {
                $testvector["ev"] = true;
        } else {
                $testvector["ev"] = false;
        }  

        $testvector["heatpump"] = isset($product["heatpump"]) ? true : false;

        $testvector["smartelec"] = isset($product["smartelec"]) ? true : false;
        $testvector["smartgas"] = isset($product["smartgas"]) ? true : false;
        $testvector["smartprepay"] = isset($product["smartprepay"]) ? true : false;
        $testvector["tradelec"] = isset($product["tradelec"]) ? true : false;
        $testvector["econ7"] = isset($product["econ7"]) ? $product["econ7"] : false;
        $testvector["econ10"] = isset($product["econ10"]) ? $product["econ10"] : false;

        $testvector["dd"] = isset($product["dd"]) ? true : false;
        $testvector["demand"] = isset($product["demand"]) ? true : false;
        $testvector["prepay"] = isset($product["is_prepay"]) ? $product["is_prepay"] : false;

        // var_dump($testvector);

        $binTestvector = encode($testvector);

        return $binTestvector;
}

function loadProducts($GSP='_A') {

        // https://www.citizensadvice.org.uk/api/customer-service-rating/get-latest

        $brandscore = array("OCTOPUS_ENERGY" => 4, "AFFECT_ENERGY" => 3, "HARPER" => 4, "COOP_ENERGY" => 4, "LONDON_POWER" => 2);
        $brandimage = array("OCTOPUS_ENERGY" => "octopus.svg", "AFFECT_ENERGY" => "affect.png", "HARPER" => "ms.svg", "COOP_ENERGY" => "coop.png", "LONDON_POWER" => "londonpower.svg");
        
        $productsjson = file_get_contents("/tmp/compare_test.json");

        $products = json_decode($productsjson, 1);
        foreach($products as $k => $product) {
                $code = $product["code"];
                $supplier = $product["brand"];
                $products[$k]['brand_image'] = "assets/" . $brandimage[$supplier];
                $products[$k]['brand_score'] = $brandscore[$supplier];      
        }
        
        return $products;
}
    
function compare($targetvector, $product, $weights) {

    $max = 24;
    $score = 0;

    $testvector = encode_product($product);
        
    $test = str_split(decbin($testvector));
    $target = str_split(decbin($targetvector));

//     echo $product["code"] . " ";

    for($i=0; $i < $max; $i++) {
            $bitmask = pow(2,$i);
        //     echo ($testvector & $bitmask) . " == " . ($targetvector & $bitmask) . " " . $score . " & ";
            
        //     if(($testvector & $bitmask) == ($targetvector & $bitmask)) {
        //         $score++;
        //         echo " match ";
        //         if($i == 1) $score = $score + $weights["green"];
        //         if($i >= 2 || $i <=8) $score = $score + $weights["price"];
        //     }

        // inclusive search will only score query or target items

            if(($testvector & $bitmask) == ($targetvector & $bitmask) && (($targetvector & $bitmask) != 0)) {
                $score++;
                // echo " match ";
                if($i == 1) $score = $score + $weights["green"];
                if($i >= 2 || $i <=8) $score = $score + $weights["price"];
            }

        //     echo decbin($bitmask) . "\r\n";
    }
        // apply weight to characteristic
        // if($k == 1) $score = $score + $weights["green"];
        // if($k >= 2 || $k <=8) $score = $score + $weights["green"];

//     echo decbin($testvector) . " : " . decbin($targetvector) . " = " . $score . "\r\n";

    // normalise the score to 100
    return $score;

}

        $results = array();

// present results based on filter
        // fixed
        // smart
        // green
        $weights["green"] = isset($_GET["greenweight"]) ? $_GET["greenweight"] : 3;
        $weights["price"] = isset($_GET["priceweight"]) ? $_GET["priceweight"] : 3;
        $weights["service"] = isset($_GET["serviceweight"]) ? $_GET["serviceweight"] : 3;

        $criteria["smart"] = isset($_GET["smart"]) ? true : false;
        $criteria["green"] = isset($_GET["green"]) ? true : false;

        $criteria["fixed"] = isset($_GET["fixed"]) ? true : false;
        $criteria["variable"] = isset($_GET["variable"]) ? true : false;
        $criteria["tou"] = isset($_GET["tou"]) ? true : false;
        $criteria["dynamic"] = isset($_GET["dynamic"]) ? true : false;

        $criteria["contract_none"] = isset($_GET["contract_none"]) ? true : false;
        $criteria["contract_12"] = isset($_GET["contract_12"]) ? true : false;
        $criteria["contract_24"] = isset($_GET["contract_24"]) ? true : false;

        $criteria["exit"] = isset($_GET["exit"]) ? true : false;

        $criteria["solar"] = isset($_GET["solar"]) ? true : false;
        $criteria["ev"] = isset($_GET["ev"]) ? true : false;
        $criteria["heatpump"] = isset($_GET["heatpump"]) ? true : false;

        $criteria["smartelec"] = isset($_GET["smartelec"]) ? true : false;
        $criteria["smartgas"] = isset($_GET["smartgas"]) ? true : false;
        $criteria["smartprepay"] = isset($_GET["smartprepay"]) ? true : false;
        $criteria["tradelec"] = isset($_GET["tradelec"]) ? true : false;
        $criteria["econ7"] = isset($_GET["econ7"]) ? true : false;
        $criteria["econ10"] = isset($_GET["econ10"]) ? true : false;

        $criteria["dd"] = isset($_GET["dd"]) ? true : false;
        $criteria["demand"] = isset($_GET["demand"]) ? true : false;
        $criteria["prepay"] = isset($_GET["prepay"]) ? true : false;

        $targetvector = encode($criteria);

        $products = loadProducts();

        foreach($products as $i => $product) {
                $product["score"] = compare($targetvector, $product, $weights);
                $results[] = $product;
        }

        $sort = isset($_GET["sort"]) ? $_GET["sort"] : "cost";

        // $scores = array_column("score", $results);
        
        if($sort == "score") {
                usort($results, "sort_score");
        } else {
                usort($results, "sort_cost");
        }

        $output["count"] = count($results);
        $output["testvector"] = $targetvector;
        $output["results"] = $results;

        $resultsjson = json_encode($output);
        header('Content-type: application/json');
        echo $_GET['callback'] . '(' . $resultsjson . ')';
        // echo count($results);
        // print_r($results);

?>