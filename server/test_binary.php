<?php

function write_small_test() {
    // binary data test
    $consumption[1500000] = 104;
    $consumption[1500010] = 133;
    $consumption[1500020] = 0;
    $consumption[1500030] = 1;
    $consumption[1500040] = 65235;

    ksort($consumption);
    $keys = array_keys($consumption);
    $maxts = max($keys);
    $mints = min($keys);

    foreach($consumption as $key => $value) {
        $valObj[] = $value;
    }

    var_dump($valObj);

    // magic header that is 0x11 for STSC data, 0x0F for consumption
    $binaryheader = pack("C*", 0x11, 0x0F);
    $binaryts = pack("N*", $mints);
    $binarydata = pack("n*", ...$valObj);
    $fp = fopen("/tmp/test.bin", "w");
    // echo $binarydata;
    fwrite($fp, $binaryheader, 2);
    fwrite($fp, $binaryts, 4);
    fwrite($fp, $binarydata);
    fclose($fp);
}

function read_consumption($filename) {
 
    $fp = fopen($filename, "r");

    if(!$fp) return -1;

    $headersig = unpack("C", fread($fp, 1));
    $headertype = unpack("C", fread($fp, 1));
    $mints = unpack("N", fread($fp, 4));

    while( $tmpval = unpack("n", fread($fp, 2))) {
        $bindata[] = $tmpval[1];
    }
    $bindata = array_slice($bindata, 0, -1);
    fclose($fp);

    // var_dump($headersig) . "\r\n";
    // var_dump($headertype) . "\r\n";
    // var_dump($mints) . "\r\n";
    // var_dump($bindata);

    return array("starttime" => $mints[1], "data" => $bindata);
}

function get_octopus_product_rates($filename, $gsp) {
    $product_json = file_get_contents($filename);
    // determine the type
    $product = json_decode($product_json, 1);

    
    $stsc_product["id"] = $product["code"];

    if(isset($product["single_register_electricity_tariffs"][$gsp]["direct_debit_monthly"])) {
        $stsc_product["type"] = "flat";
        $stsc_product["paymenttype"] = "dd";
        $product_detail = $product["single_register_electricity_tariffs"][$gsp]["direct_debit_monthly"];
        $stsc_product["standingcharge"] = $product_detail["standing_charge_inc_vat"];
        $stsc_product["rate"]["unitrate"] = $product_detail["standard_unit_rate_inc_vat"];
    } else if(isset($product["single_register_electricity_tariffs"][$gsp]["prepayment"])) {
        $stsc_product["type"] = "flat";
        $stsc_product["paymenttype"] = "prepay";
        $product_detail = $product["single_register_electricity_tariffs"][$gsp]["prepayment"];
        $stsc_product["standingcharge"] = $product_detail["standing_charge_inc_vat"];
        $stsc_product["rate"]["unitrate"] = $product_detail["standard_unit_rate_inc_vat"];
    } else if(isset($product["single_register_electricity_tariffs"][$gsp]["porob"])) {
        $stsc_product["type"] = "flat";
        $stsc_product["paymenttype"] = "ondemand";
        $product_detail = $product["single_register_electricity_tariffs"][$gsp]["porob"];
        $stsc_product["standingcharge"] = $product_detail["standing_charge_inc_vat"];
        $stsc_product["rate"]["unitrate"] = $product_detail["standard_unit_rate_inc_vat"];
    }

    if(isset($product["dual_register_electricity_tariffs"][$gsp]["direct_debit_monthly"])) {
        $stsc_product["type"] = "econx";
        $product_detail = $product["dual_register_electricity_tariffs"][$gsp]["direct_debit_monthly"];
        $stsc_product["standingcharge"] = $product_detail["standing_charge_inc_vat"];
        $stsc_product["rate"]["day_unitrate"] = $product_detail["day_unit_rate_inc_vat"];
        $stsc_product["rate"]["night_unitrate"] = $product_detail["night_unit_rate_inc_vat"];
    } else if(isset($product["dual_register_electricity_tariffs"][$gsp]["porob"])) {
        $stsc_product["type"] = "econx";
        $product_detail = $product["dual_register_electricity_tariffs"][$gsp]["porob"];
        $stsc_product["standingcharge"] = $product_detail["standing_charge_inc_vat"];
        $stsc_product["rate"]["day_unitrate"] = $product_detail["day_unit_rate_inc_vat"];
        $stsc_product["rate"]["night_unitrate"] = $product_detail["night_unit_rate_inc_vat"];
    }

    if(substr($product["code"], 0, 2) == "GO") {
        $stsc_product["type"] = "tou";
        $product_detail = $product["single_register_electricity_tariffs"][$gsp]["direct_debit_monthly"];
        $stsc_product["standingcharge"] = $product_detail["standing_charge_inc_vat"];
        $unitratesURL = "https://api.octopus.energy/v1/products/" . $product["code"] . "/electricity-tariffs/" . $product_detail["code"] . "/standard-unit-rates";
        $tariff_json = file_get_contents($unitratesURL);
        $tariff = json_decode($tariff_json, 1);
        $stsc_product["rate"] = array($tariff["results"][0], $tariff["results"][1]);
    }
    
    return $stsc_product;
}

// $mydata = read_consumption("/tmp/test.bin");
$productrates = get_octopus_product_rates("/tmp/product_GO-5H-2230.json", "_A");

var_dump($productrates);

?>